import math
import re


def Counting(string):
    line = {'Символы': 0, 'Пробелы': 0, 'Запятые': 0}

    for i in string:
        if ' ' in i:
            line['Пробелы'] += 1
        elif ',' in i:
            line['Запятые'] += 1
        else:
            line['Символы'] += 1

    print("Символов: {:d} \nЗапятых: {:d} \nПробелов: {:d}\n".
          format(line['Символы'], line['Запятые'], line['Пробелы']))


def Calculator():
    while True:
        first = input("Введите первое число: ")
        if re.fullmatch(r'^-?[0-9]+\.?[0-9]*$', first):
            break

    print('  + : Сложение\n'
          '  - : Вычитание\n'
          '  * : Умножение\n'
          '  / : Деление\n'
          '  // : Деление без остатка\n'
          '  % : Нахождение остатка от деления\n'
          '  ** : Возведение в степень\n'
          '  log10 : Логарифм по основанию 10\n'
          '  log2 : Логарифм по основанию 2\n'
          '  cos : Косинус числа {:s}\n'
          '  sin : Синус числа {:s}\n'
          '  square : Площадь поверхности параллепипеда\n'
          '  perimeter : Периметр параллепипеда\n'
          '  0 : Выход'.format(first, first))
    choice = input("Выберите нужную функцию: ")

    # конвертируем введенное число в цифру
    num = int(first) if first.isdecimal() else float(first)

    match choice:
        case '0':
            print('Вы вышли из калькулятора')
        case 'log10':
            if num < 0:
                print('\nЛогарифма отрицательного числа не существует!\n')
            else:
                result = math.log10(num)
                print('log10({:s}) = {:s}'.format(first, str(result)))

        case 'log2':
            if num < 0:
                print('\nЛогарифма отрицательного числа не существует!\n')
            else:
                result = math.log2(num)
                print('log2({:s}) = {:s}'.format(first, str(result)))

        case 'cos':
            result = math.cos(num)
            print('cos({:s}) = {:s}'.format(first, str(result)))
        case 'sin':
            result = math.sin(num)
            print('sin({:s}) = {:s}'.format(first, str(result)))
        case _:
            while True:
                second = input("\nВведите второе число: ")
                if re.fullmatch(r'^-?[0-9]+\.?[0-9]*$', second):
                    break

            num2 = int(second) if first.isdecimal() else float(second)

            match choice:
                case '+':
                    print("{:s} + {:s} = {:s}".format(first, second, str(num + num2)))
                case '-':
                    print("{:s} - {:s} = {:s}".format(first, second, str(num - num2)))
                case '*':
                    print("{:s} * {:s} = {:s}".format(first, second, str(num * num2)))
                case '**':
                    print("{:s}^{:s} = {:s}".format(first, second, str(num ** num2)))
                case '/':
                    print("{:s} / {:s} = {:s}".format(first, second, str(num / num2)))
                case '//':
                    print("Результат деления без остатка {:s} на {:s} = {:s}".format(first, second, str(num // num2)))
                case '%':
                    print("Остаток от деления {:s} на {:s} = {:s}".format(first, second, str(num / num2)))
                case _:
                    while True:
                        third = input("\nВведите третье число: ")
                        if re.fullmatch(r'^-?[0-9]+\.?[0-9]*$', third):
                            break

                    num3 = int(third) if first.isdecimal() else float(third)

                    match choice:
                        case 'square':
                            if (checker(num, num2, num3)):
                                print('Площадь параллепипеда со сторонами {:s}, {:s}, {:s} = {:s}'
                                      .format(first, second, third, str(square(num, num2, num3))))
                            else:
                                print('Параллепипеда с отрицательным значением стороны не существует!')
                        case 'perimeter':
                            if (checker(num, num2, num3)):
                                print('Периметр параллепипеда со сторонами {:s}, {:s}, {:s} = {:s}'
                                      .format(first, second, third, str(perimetr(num, num2, num3))))
                            else:
                                print('Параллепипеда с отрицательным значением стороны не существует!')
                        case _:
                            print('Вы ввели неверную функцию!')


def Matrix():
    while True:
        column = input("Введите количество столбцов: ")
        if column.isdecimal():
            column = int(column)
            break
    while True:
        row = input("Введите количество строк: ")
        if row.isdecimal():
            row = int(row)
            break
    while True:
        start = input("Введите начальное значение: ")
        if start.isdecimal():
            start = int(start)
            break
    while True:
        step = input("Введите шаг, на который будет увеличиваться предыдущее значение: ")
        if step.isdecimal():
            step = int(step)
            break

    matrix = [[0 for x in range(column)] for y in range(row)]

    for x in range(row):
        for y in range(column):
            matrix[x][y] = start
            start += step

    for i in matrix:
        print(*i)


square = lambda x, y, z: 2 * (x * y + y * z + z * x)

perimetr = lambda a, b, c: 4 * a + 4 * b + 5 * c

checker = lambda a, b, c: True if a > 0 and b > 0 and c > 0 else False
