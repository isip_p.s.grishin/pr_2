while True:
    column = input("Введите количество столбцов: ")
    if column.isdecimal():
        column = int(column)
        break
while True:
    row = input("Введите количество строк: ")
    if row.isdecimal():
        row = int(row)
        break
while True:
    start = input("Введите начальное значение: ")
    if start.isdecimal():
        start = int(start)
        break
while True:
    step = input("Введите шаг, на который будет увеличиваться предыдущее значение: ")
    if step.isdecimal():
        step = int(step)
        break

matrix = [[0 for x in range(column)] for y in range(row)]

for x in range(row):
    for y in range(column):
        matrix[x][y] = start
        start += step

for i in matrix:
    print(*i)
input()
