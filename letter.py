string = input("\nВведите строку: ")
line = {'Символы': 0, 'Пробелы': 0, 'Запятые': 0}

for i in string:
    if ' ' in i:
        line['Пробелы'] += 1
    elif ',' in i:
        line['Запятые'] += 1
    else:
        line['Символы'] += 1

print("Символов: {:d} \nЗапятых: {:d} \nПробелов: {:d}\n".
      format(line['Символы'], line['Запятые'], line['Пробелы']))
input()
